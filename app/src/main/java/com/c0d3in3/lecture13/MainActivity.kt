package com.c0d3in3.lecture13

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), CustomAdapter.CustomInterface{

    companion object{
        const val REQUEST_CODE = 22
    }
    private val itemsList = arrayListOf<ItemModel>()
    private val adapter = CustomAdapter(itemsList, this)

    private val images = listOf<Int>(R.mipmap.ic_doggo_1, R.mipmap.ic_doggo_2, R.mipmap.ic_doggo_3, R.mipmap.ic_doggo_4, R.mipmap.ic_doggo_5,
        R.mipmap.ic_doggo_6, R.mipmap.ic_doggo_7, R.mipmap.ic_doggo_8)

    private var doggos = 1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init(){
        recyclerView.layoutManager = GridLayoutManager(this, 2)
        recyclerView.adapter = adapter
        addDataToList()

        addButton.setOnClickListener {
            addItem()
            recyclerView.scrollToPosition(itemsList.size-1)
        }
    }

    private fun addDataToList(){
        for(i in 0..5){
            itemsList.add(ItemModel(images.random(), "doggo_$doggos", "Click image to edit\nLong click to remove"))
            doggos++
        }
    }

    override fun editItem(position : Int) {
        val intent = Intent(this, EditItemActivity::class.java)
        val parcel = ItemModelParcelable(position, itemsList[position].title, itemsList[position].description)
        intent.putExtra("item", parcel)
        startActivityForResult(intent, REQUEST_CODE)
    }

    override fun removeItem(position: Int): Boolean {
        itemsList.removeAt(position)
        adapter.notifyItemRemoved(position)
        return true
    }

    private fun addItem() {
        val randomImage = images.random()
        itemsList.add(ItemModel(randomImage, "doggo_$doggos", "Random doggo"))
        adapter.notifyItemInserted(itemsList.size-1)
        doggos++
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if(resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE){
            val itemModel = data?.extras?.get("item") as ItemModelParcelable
            itemsList[itemModel.position].title = itemModel.title
            itemsList[itemModel.position].description = itemModel.description
            adapter.notifyItemChanged(itemModel.position)
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

}
