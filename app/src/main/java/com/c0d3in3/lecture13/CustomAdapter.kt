package com.c0d3in3.lecture13

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_layout.view.*


class CustomAdapter(private val items : ArrayList<ItemModel>, listener: CustomInterface) : RecyclerView.Adapter<CustomAdapter.ViewHolder>(){

    interface CustomInterface{
        fun editItem(position : Int)
        fun removeItem(position: Int) : Boolean
    }

    private val callback = listener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false))
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        fun onBind(){
            val model = items[adapterPosition]
            itemView.titleTextView.text = model.title
            itemView.imageViewButton.setImageResource(model.image)
            itemView.descriptionTextView.text = model.description

            itemView.imageViewButton.setOnClickListener {
                callback.editItem(adapterPosition)
            }
            itemView.imageViewButton.setOnLongClickListener {
                callback.removeItem(adapterPosition)
            }
        }
    }


}