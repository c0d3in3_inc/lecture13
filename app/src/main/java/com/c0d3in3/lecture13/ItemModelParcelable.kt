package com.c0d3in3.lecture13

import android.os.Parcel
import android.os.Parcelable

class ItemModelParcelable(val position : Int, val title : String, val description : String) : Parcelable {
    constructor(parcel: Parcel) : this(
        parcel.readInt(),
        parcel.readString()!!,
        parcel.readString()!!
    ) {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(position)
        parcel.writeString(title)
        parcel.writeString(description)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ItemModelParcelable> {
        override fun createFromParcel(parcel: Parcel): ItemModelParcelable {
            return ItemModelParcelable(parcel)
        }

        override fun newArray(size: Int): Array<ItemModelParcelable?> {
            return arrayOfNulls(size)
        }
    }
}