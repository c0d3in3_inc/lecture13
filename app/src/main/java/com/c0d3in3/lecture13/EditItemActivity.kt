package com.c0d3in3.lecture13

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_edit_item.*

class EditItemActivity : AppCompatActivity() {

    private var position = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_item)

        init()
    }

    private fun init(){
        val itemModel = intent.extras?.get("item") as ItemModelParcelable
        titleEditText.text = itemModel.title.toEditable()
        descEditText.text = itemModel.description.toEditable()
        position = itemModel.position

        saveButton.setOnClickListener {
            if(titleEditText.text.isEmpty() || descEditText.text.isEmpty()) return@setOnClickListener Toast.makeText(this,"Please fill all the fields!", Toast.LENGTH_LONG).show()
            saveChanges()
        }

        cancelButton.setOnClickListener {
            cancelChanges()
        }
    }

    private fun saveChanges(){
        val itemModel = ItemModelParcelable(position, titleEditText.text.toString(), descEditText.text.toString())
        intent.putExtra("item", itemModel)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun cancelChanges(){
        setResult(Activity.RESULT_CANCELED, intent)
        finish()
    }

    private fun String.toEditable(): Editable =  Editable.Factory.getInstance().newEditable(this)
}
